package assignment1



type CountrySpecies struct{
	Species string `json:"species"`
	SpeciesKey 	int `json:"speciesKey"`
}

type Results struct{
	Results   []CountrySpecies `json:"results`
}

type Country struct{
	Code 				string `json:"alpha2code"`
	Countryname 		string `json:"name"`
	Countryflag 		string `json:"flag"`
	Species				[]string
	SpeciesKey			[]int
	
}

type Species struct{
	Key			   int `json:"key"`
	Kingdom 	   string `json:"kingdom"`
	Phylum 		   string `json:"phylum"`
	Order 		   string `json:"order"`
	Family 		   string `json:"family"`
	Genus 		   string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year 		   string `json:"bracketYear"`
}


type Diag struct{
	Gbif 		  int 
	RestCountries int 
	Version 	  string 
	Uptime 		  float64
}
