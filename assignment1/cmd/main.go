package main

import (
	"assignment1"
	"fmt"
	"log"
	"net/http"
	"time"
	"os"
)

var StartTime = time.Now() // sets StartTime

func main() {

	assignment1.StartTime = StartTime // send StartTime
	

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc("/", assignment1.HandlerNil)
	http.HandleFunc("/conservation/v1/country/", assignment1.HandlerCountry) // runs handelr function
	http.HandleFunc("/conservation/v1/species/", assignment1.HandlerSpecies) // runs handelr function
 	http.HandleFunc("/conservation/v1/diag/", assignment1.HandlerDiag) // runs handelr function
	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
