package assignment1

import (
	"time"
	"log"
	"encoding/json" 
	"fmt"
	"net/http"
	"strings"
)

var StartTime time.Time //creat global time variable


func DoRequest (Client *http.Client, API string)*http.Response{  // Request api
	req, err := http.NewRequest(http.MethodGet, API, nil)
	if err != nil{
		log.Fatal(err)
	}

	resp, err := Client.Do(req)
	if err != nil{
		log.Fatal(err)
	}

	return resp
}

func HandlerNil(w http.ResponseWriter, r *http.Request) {  //standar default respons 
	fmt.Println("Default Handler: Invalid request received.")
	http.Error(w, "Invalid request", http.StatusBadRequest)
}

func HandlerCountry(w http.ResponseWriter, r *http.Request) {
	
	var Co Country // creats struct Country
	var Re Results // Creats struct Results

	parts := strings.Split(r.URL.Path,"/") // finds url code
	if len(parts) != 5{
		status:= http.StatusBadRequest
		http.Error(w,"country/{:country_identifier}{?limit={:limit}}", status)
		return
	}

	limit := r.URL.Query().Get("limit") // gets limit
	if limit == ""{ // check if there are choosen an limit
		limit = "30" // defoult limit
	}

	COAPIURL := strings.Join([]string{"https://restcountries.eu/rest/v2/alpha", parts[4]}, "/") // creates api url for contries
	GBIFAPIURL := strings.Join([]string{"http://api.gbif.org/v1/occurrence/search?country=", parts[4],"&limit=", limit}, "") // creates api url for species
	Client := http.DefaultClient

	resp := DoRequest(Client,COAPIURL) // get data from url

	err := json.NewDecoder(resp.Body).Decode(&Co) // decode data to country
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp2 := DoRequest(Client, GBIFAPIURL)

	err = json.NewDecoder(resp2.Body).Decode(&Re) //decod data to results
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for i := range Re.Results { // loops true results
		Co.Species = append(Co.Species, Re.Results[i].Species) // sets contry Species to be string of all species from results
		Co.SpeciesKey = append(Co.SpeciesKey, Re.Results[i].SpeciesKey) // sets country SpeciesKey to be string of all specieskes from results
	}

	http.Header.Add(w.Header(), "Content-Type", "application/json") // makes the print look good
	
	json.NewEncoder(w).Encode(Co) // Encods data	
}

func HandlerSpecies(w http.ResponseWriter, r *http.Request) {
	
	var Sp Species // creats struct Sp

	parts := strings.Split(r.URL.Path,"/") // finds url key
	if len(parts) != 5{
		status:= http.StatusBadRequest
		http.Error(w,"species/{:speciesKey}", status)
		return
	}

	GBIFAPIURL := strings.Join([]string{"http://api.gbif.org/v1/species", parts[4]}, "/") // creates api url for species
	GBIFAPIURL2 := strings.Join([]string{GBIFAPIURL,"name"}, "/")  // creats api url for year
	Client := http.DefaultClient

	resp:= DoRequest(Client,GBIFAPIURL)  // get data from url
	
	err := json.NewDecoder(resp.Body).Decode(&Sp) // decode data to species
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	

	resp2 := DoRequest(Client,GBIFAPIURL2) // get data for url

	err = json.NewDecoder(resp2.Body).Decode(&Sp) // decode data to species
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}


	http.Header.Add(w.Header(), "Content-Type", "application/json")  // makes the print look good

	json.NewEncoder(w).Encode(Sp) // encode the data	
}


func HandlerDiag(w http.ResponseWriter, r *http.Request) {

	var D Diag // creats struct Diag

	resp, err := http.Get ("http://api.gbif.org/v1/") // gets API
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	
	D.Gbif = resp.StatusCode // shows statuscode for Gbif API
	

	resp, err = http.Get ("https://restcountries.eu/rest/v2") // gets API
	if err != nil{
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	
	D.RestCountries = resp.StatusCode // shows statuscode for RestCountris API


	D.Version = "v1" // shows version runnig


	elapse := time.Since(StartTime) // sets uptime
	D.Uptime = elapse.Seconds() //shows time in seconds from start


	http.Header.Add(w.Header(), "Content-Type", "application/json") // makes the print look good

	json.NewEncoder(w).Encode(D) // encode the data
}